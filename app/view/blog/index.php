<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial- scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Halaman <?= $data["judul"]; ?></title>
</head>

<body>
<div class="container mt-5">
   <div class="row">
       <div class="col-6">
           <h3>Blog</h3>
               <?php foreach($data["blog"] as $blog) :?>
               <ul>
                   <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?= $blog["judul"]; ?>
                    <a href="<?= BASE_URL;?>/blog/detail/<?= $blog['id'];?>" class="badge badge-dark">detail</a>
                    </li>
                  
               </ul>
           <?php endforeach; ?>
       </div>
   </div>
</div>
</body>

</html>